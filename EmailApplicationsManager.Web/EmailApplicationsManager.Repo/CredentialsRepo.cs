﻿using EmailApplicationsManager.Repo.Contracts;
using EmailApplicationsManager.Web.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Repo
{
	public class CredentialsRepo : ICredentialsRepo
	{
		private readonly EmailAppContext dbContext;

		public CredentialsRepo(EmailAppContext context)
		{
			dbContext = context;
		}

		public async Task<bool> AccessTokenExist()
		{
			var result = await dbContext.GmailCredentials.FirstOrDefaultAsync();

			if ( result != null)
			{
				return true;
			}

			return false;
		}
	}
}
