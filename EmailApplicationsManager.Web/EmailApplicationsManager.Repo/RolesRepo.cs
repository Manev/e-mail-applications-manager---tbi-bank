﻿using EmailApplicationsManager.Data.Abstract;
using EmailApplicationsManager.Repo.Contracts;
using EmailApplicationsManager.Web.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EmailApplicationsManager.Repo
{
	public class RolesRepo : IRolesRepo
	{
		private readonly EmailAppContext dbContext;

		public RolesRepo(EmailAppContext context)
		{
			dbContext = context;
		}

		public IEnumerable<SelectListItem> GetRoles()
		{
			IEnumerable<SelectListItem> Roles = dbContext.Roles.Select(r => new SelectListItem { Text = r.Name , Value = r.Name });

			return Roles;
		}

	}
}
