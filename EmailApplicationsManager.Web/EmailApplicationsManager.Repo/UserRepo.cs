﻿using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Repo.Contracts;
using EmailApplicationsManager.Web.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmailApplicationsManager.Repo
{
	public class UserRepo : IUserRepo
	{
		private readonly EmailAppContext dbContext;

		public UserRepo(EmailAppContext context)
		{
			dbContext = context;
		}

		public User GetUser(string email)
		{
			var user = dbContext.Users.FirstOrDefault(u => u.Email == email);

			return user;
		}

		public string GetUserName(string userId)
		{
			var user = dbContext.Users.FirstOrDefault(p => p.Id == userId);

			return user.UserName;
		}

		public void UpdateUserInfo(User user)
		{
			user.HasLogged = true;
			dbContext.SaveChanges();
		}
	}
}
