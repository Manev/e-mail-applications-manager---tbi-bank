﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Repo.Contracts
{
	public interface ICredentialsRepo
	{
		Task<bool> AccessTokenExist();
	}
}
