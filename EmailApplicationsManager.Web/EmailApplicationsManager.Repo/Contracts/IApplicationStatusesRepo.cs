﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmailApplicationsManager.Repo.Contracts
{
	public interface IApplicationStatusesRepo
	{
		IEnumerable<SelectListItem> GetAppStatuses();
	}
}
