﻿using EmailApplicationsManager.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmailApplicationsManager.Repo.Contracts
{
	public interface IUserRepo
	{
		User GetUser(string email);

		void UpdateUserInfo(User user);

		string GetUserName(string userId);
	}
}
