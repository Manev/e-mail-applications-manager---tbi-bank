﻿using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Repo.Contracts;
using EmailApplicationsManager.Web.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmailApplicationsManager.Repo
{
	public class ApplicationStatusesRepo : IApplicationStatusesRepo
	{
		private readonly EmailAppContext context;

		public ApplicationStatusesRepo(EmailAppContext context)
		{
			this.context = context;
		}

		public IEnumerable<SelectListItem> GetAppStatuses()
		{
			IEnumerable<SelectListItem> Statuses = context.ApplicationStatuses.Where(p=>p.AppStatusName == ApplicationStatuseEnum.Approved.ToString() || p.AppStatusName == ApplicationStatuseEnum.Rejected.ToString()).Select(r => new SelectListItem
			{
				Text = r.AppStatusName,
				Value = r.AppStatusName
			});

			return Statuses;
		}
	}
}
