﻿using System.Threading.Tasks;

namespace EmailApplicationsManager.Services.Update
{
	public interface IUpdateServices
	{
		Task<string> GetBody(string gmailId);

		void ChangeStatus(int mailId, string newStatus, string body);

		void AddCustomerInformation(int mailId, string EGN, string phoneNumber, string userId);

		void ApplicationClosing(int mailId, string appStatus, string userId);
	}
}