﻿using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Services.Cypher;
using EmailApplicationsManager.Services.DTO;
using EmailApplicationsManager.Services.Services;
using EmailApplicationsManager.Services.TestGmail;
using EmailApplicationsManager.Web.Data;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Services.Update
{
	public class UpdateServices : IUpdateServices
	{
		private readonly EmailAppContext context;
		private readonly IEmailServices service;
		private readonly ICredentialsManager credentials;
		private readonly IStringCipher cipher;

		public UpdateServices(EmailAppContext context, IEmailServices service, ICredentialsManager credentials, IStringCipher cipher)
		{
			this.context = context;
			this.service = service;
			this.credentials = credentials;
			this.cipher = cipher;
		}

		public async Task<string> GetBody(string gmailId)
		{
			var userData = await credentials.GetAsync();

			var client = new HttpClient();

			client.DefaultRequestHeaders.Add("Authorization", "Bearer " + userData.AccessToken);

			var res = await client.GetAsync($"https://www.googleapis.com/gmail/v1/users/me/messages/{gmailId}");
			var content = await res.Content.ReadAsStringAsync();

			var firstMessage = JsonConvert.DeserializeObject<GmailMessagesDTO>(await res.Content.ReadAsStringAsync());

			var bodyManager = service.EmailContentProvider(firstMessage);

			string body = bodyManager.Last();

			return body;
		}


		public void ChangeStatus(int mailId, string newStatus, string body)
		{
			var email = this.context.Emails.Include(p => p.Status).FirstOrDefault(p => p.Id == mailId);


			var status = this.context.EmailStatuses.FirstOrDefault(p => p.StatusName.Replace(" ","").ToLower() == newStatus.Replace(" ", "").ToLower());

			StatusFlowValidation(email, status, body);
		}

		public void AddCustomerInformation(int mailId , string EGN , string phoneNumber , string userId)
		{
			var email = this.context.Emails.FirstOrDefault(p => p.Id == mailId);

			email.EGN = cipher.Encrypt(EGN);
			email.PhoneNumber = phoneNumber;
			email.UserId = userId; 
		

			this.context.SaveChanges();
		}

		private Email StatusFlowValidation(Email email , EmailStatus status , string body)
		{
			if (status.StatusName == EmailStatusesEnum.New.ToString() && email.Status.StatusName.Replace(" ", "").ToLower() == EmailStatusesEnum.NotReviewed.ToString().ToLower())
			{
				email.Body = cipher.Encrypt(body);
			}

			if (status.StatusName.Replace(" ", "").ToLower() == EmailStatusesEnum.NotReviewed.ToString().ToLower() && email.Status.StatusName == EmailStatusesEnum.New.ToString())
			{
				email.Body = string.Empty;
			}

			if (status.StatusName == EmailStatusesEnum.New.ToString() && email.Status.StatusName == EmailStatusesEnum.Open.ToString())
			{
				email.EGN = string.Empty;
				email.PhoneNumber = string.Empty;
			}

			if (status.StatusName == EmailStatusesEnum.New.ToString() && email.Status.StatusName == EmailStatusesEnum.Closed.ToString())
			{
				email.EGN = string.Empty;
				email.PhoneNumber = string.Empty;
				email.SetInTerminalState = null;
				email.AppStatusId = 3;
			}

			if (status.StatusName == EmailStatusesEnum.Closed.ToString() && email.Status.StatusName == EmailStatusesEnum.Open.ToString())
			{
				email.SetInTerminalState = DateTime.Now;
			}

			email.StatusId = status.Id;
			email.SetInCurrentStatusOn = DateTime.Now;

			this.context.SaveChanges();

			return email;
		}

		public void ApplicationClosing(int mailId , string appStatus, string userId)
		{
			var email = this.context.Emails
				.Include(p=>p.AppStatus)
				.Include(p=>p.User)
				.FirstOrDefault(p => p.Id == mailId);

			var status = this.context.ApplicationStatuses.FirstOrDefault(p => p.AppStatusName == appStatus);

			email.AppStatusId = status.Id;
			email.UserId = userId;

			this.context.SaveChanges();
		}


	}
}
