﻿using EmailApplicationsManager.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmailApplicationsManager.Services.AuditLogger
{
	public interface IAuditLogger
	{
		void Log(string userId, int emailId, string info, string lastStatus, string newStatus);

		IReadOnlyCollection<Log> GetLogs();
	}
}
