﻿using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Web.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmailApplicationsManager.Services.AuditLogger
{
	public class AuditLogger : IAuditLogger
	{
		private readonly EmailAppContext dbContext;

		public AuditLogger(EmailAppContext dbContext)
		{
			this.dbContext = dbContext;
		}

		public void Log(string userId, int emailId, string info, string lastStatus, string newStatus)
		{
			Log log = new Log
			{
				Date = DateTime.Now,
				UserId = userId,
				EmailId = emailId,
				Info = info,
				LastStatus = lastStatus,
				NewStatus = newStatus
			};

			dbContext.Logs.Add(log);
			dbContext.SaveChanges();
		}

		public IReadOnlyCollection<Log> GetLogs()
		{
			var logs = dbContext.Logs
				.Include(l => l.User)
				.Include(l => l.Email)
				.ToList();

			return logs;
		}
	}
}
