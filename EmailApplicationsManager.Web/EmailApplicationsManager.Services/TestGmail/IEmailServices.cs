﻿using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Services.DTO;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Services.TestGmail
{
	public interface IEmailServices
	{
		List<string> EmailContentProvider(GmailMessagesDTO message);

		Email GetEmail(int mailId);

		Email AddEmailToDB(List<string> args);

		IReadOnlyCollection<Email> ListEmailsFromDB(string statusName);

		Task ListMessages(GmailCredentials userData);

		Task<GmailCredentialsDTO> RenewAccessTokenAsync(string refreshToken);

		Task<GmailCredentials> UpdateAccessToken(GmailCredentialsDTO credentials);

		IReadOnlyCollection<Email> ListEmailsForOperator(string userId , string status);

		string UserName(string userId);

		IReadOnlyCollection<Email> ListEmailsForManagers();
	}
}
