﻿using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Services.Cypher;
using EmailApplicationsManager.Services.DTO;
using EmailApplicationsManager.Services.Services;
using EmailApplicationsManager.Web.Data;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Services.TestGmail
{
	public class EmailServices : IEmailServices
	{
		private const string htmlAttachment = "multipart/mixed";
		private const string plain = "text/plain";
		private const string html = "multipart/alternative";
		private readonly EmailAppContext context;
		private IStringCipher cipher;
		private readonly ICredentialsManager credentials;
		private readonly IDictionary<string, string> bodyStore = new Dictionary<string, string>();  

		public EmailServices(EmailAppContext context, IStringCipher cipher, ICredentialsManager credentials)
		{
			this.context = context;
			this.cipher = cipher;
			this.credentials = credentials;
		}

		public async Task ListMessages(GmailCredentials userData)
		{
			var client = new HttpClient();

			client.DefaultRequestHeaders.Add("Authorization", "Bearer " + userData.AccessToken);

			var res = await client.GetAsync("https://www.googleapis.com/gmail/v1/users/me/messages?includeSpamTrash=true");
			
			var content = await res.Content.ReadAsStringAsync();

			var gmailMessagesList = JsonConvert.DeserializeObject<GmailMessagesListDTO>(await res.Content.ReadAsStringAsync());

			int newMailsCount = gmailMessagesList.Count - await this.context.Emails.CountAsync();

			var status = await this.context.EmailStatuses.FirstOrDefaultAsync(s => s.StatusName == "Not Reviewed");

			while (true)
			{
				if (newMailsCount <= 0)
				{
					break;
				}

				var messageId = gmailMessagesList.Messages.First().Id;
				var messageAlreadyInDb = await this.context.Emails.AnyAsync(e => e.GmailId == messageId);

				if (messageAlreadyInDb)
				{
					gmailMessagesList.Messages.RemoveAt(0);
					continue;
				}

				var str = new StringBuilder("https://www.googleapis.com/gmail/v1/users/me/messages/")
				.Append(messageId)
				.Append("?format=full");
				res = await client.GetAsync(str.ToString());
				content = await res.Content.ReadAsStringAsync();

				var firstMessage = JsonConvert.DeserializeObject<GmailMessagesDTO>(await res.Content.ReadAsStringAsync());

				var email = AddEmailToDB(EmailContentProvider(firstMessage));

				if (email.AttachmentsCount > 0)
				{
					for (int i = 1; i < firstMessage.Payload.Parts.Count; i++)
					{
						AddAttachmentToDB(firstMessage, email, i);
					}
				}

				 gmailMessagesList.Messages.RemoveAt(0);

				newMailsCount--;
			}
			context.SaveChanges();
		}

		public async Task<GmailCredentialsDTO> RenewAccessTokenAsync(string refreshToken)
		{
			var client = new HttpClient();
			var content = new FormUrlEncodedContent(new[]
			{
				new KeyValuePair<string,string>("client_id","708903751266-ir6gpg7ck4d1okr8mimfqmqtj0qqeqjg.apps.googleusercontent.com"),
				new KeyValuePair<string,string>("client_secret","g8XjzM9InQV4siZ66xTtPJDZ"),
				new KeyValuePair<string,string>("refresh_token", refreshToken),
				new KeyValuePair<string,string>("grant_type","refresh_token")
			});

			var res = await client.PostAsync("https://oauth2.googleapis.com/token", content);

			if (!res.IsSuccessStatusCode)
			{
				throw new Exception();
			}
			else
			{
				var userDataDTO = JsonConvert.DeserializeObject<GmailCredentialsDTO>(await res.Content.ReadAsStringAsync());
				return userDataDTO;
			}
		}

		public async Task<GmailCredentials> UpdateAccessToken(GmailCredentialsDTO credentials)
		{
			var userData = await context.GmailCredentials.FirstOrDefaultAsync();
			userData.AccessToken = credentials.AccessToken;
			userData.ExpiresOn = DateTime.Now.AddHours(1.00);

			await context.SaveChangesAsync();
			return userData;
		}


		public byte[] FromBase64ForUrlString(string base64ForUrlInput)
		{
			int padChars = (base64ForUrlInput.Length % 4) == 0 ? 0 : (4 - (base64ForUrlInput.Length % 4));
			StringBuilder result = new StringBuilder(base64ForUrlInput, base64ForUrlInput.Length + padChars);
			result.Append(string.Empty.PadRight(padChars, '='));
			result.Replace('-', '+');
			result.Replace('_', '/');

			return Convert.FromBase64String(result.ToString());
		}

		public Email AddEmailToDB(List<string> args)
		{
			var status = this.context.EmailStatuses.FirstOrDefault(s => s.StatusName == "Not Reviewed");

			string gmailId = args[0];
			string sender =cipher.Encrypt(args[1]);
			DateTime date = DateTime.Parse(args[2]);
			string subject = args[3];
			int attachmentCount = int.Parse(args[4]);
			int attachmentSize = int.Parse(args[5]);

			Email email = new Email
			{
				StatusId = status.Id,
				GmailId = gmailId,
				Sender = sender,
				DateReceived = date,
				Subject = subject,
				Body = string.Empty,
				AttachmentsCount = attachmentCount,
				AttachmentsSize = attachmentSize,
				InitialRegistrationInSystemOn = DateTime.Now,
				SetInCurrentStatusOn = DateTime.Now,
				AppStatusId = 3
			};

			this.context.Emails.Add(email);

			this.context.SaveChanges();

			return email;
		}

		public Attachment AddAttachmentToDB(GmailMessagesDTO message, Email email, int i)
		{
			;

			Attachment attachment = new Attachment
			{
				EmailId = email.Id,
				GmailId = message.Id,
				FileName = message.Payload.Parts[i].FileName,
				SizeInMb = (double)message.Payload.Parts[i].Body.SizeInBytes / (1024 * 1024)
			};

			this.context.Attachments.Add(attachment);

			return attachment;
		}
		public List<string> EmailContentProvider(GmailMessagesDTO message)
		{
			List<string> args = new List<string>();
			var sender = message.Payload.Headers.FirstOrDefault(x => x.Name == "From");
			var date = DateParser(message.InternalDate);
			var subject = message.Payload.Headers.FirstOrDefault(x => x.Name == "Subject");
			args.Add(message.Id);
			args.Add(sender.Value);
			args.Add(date);
			args.Add(subject.Value);

			switch (message.Payload.MimeType.ToString())
			{

				case htmlAttachment:

					args.Add((message.Payload.Parts.Count - 1).ToString());
					int? sum = 0;

					for (int i = 1; i < message.Payload.Parts.Count; i++)
					{
						sum += message.Payload.Parts[i].Body.SizeInBytes;
					}
					args.Add(sum.ToString());
					args.Add(sum.ToString());
					if (message.Payload.Parts[0].Parts == null)
					{
						byte[] data1 = FromBase64ForUrlString(message.Payload.Parts[0].Body.Data);
						string decodedString1 = Encoding.UTF8.GetString(data1);
						args.Add(decodedString1);
					}
					else
					{
						byte[] data1 = FromBase64ForUrlString(message.Payload.Parts[0].Parts[0].Body.Data);
						string decodedString1 = Encoding.UTF8.GetString(data1);
						args.Add(decodedString1);
					}
					break;

				case plain:
					byte[] data = FromBase64ForUrlString(message.Payload.Body.Data);
					string decodedString = Encoding.UTF8.GetString(data);
					sum = 0;
					args.Add(sum.ToString());
					args.Add(sum.ToString());
					args.Add(decodedString);
					break;

				case html:
					data = FromBase64ForUrlString(message.Payload.Parts[0].Body.Data);
					decodedString = Encoding.UTF8.GetString(data);
					sum = 0;
					args.Add(sum.ToString());
					args.Add(sum.ToString());
					args.Add(decodedString);
					break;

				default:
					break;
			}

			return args;
		}

		public Email GetEmail(int mailId)
		{
			var email = context.Emails
				.Include(p => p.Attachments)
				.Include(p => p.Status)
				.Include(p=>p.AppStatus)
				.FirstOrDefault(x => x.Id == mailId);

			if (email == null)
			{
				throw new ArgumentException();
			}

			return email;
		}

		private string DateParser(long? gmailDate)
		{
			var gmail_date = (long)gmailDate;

			var to_date = DateTimeOffset.FromUnixTimeMilliseconds(gmail_date).DateTime;

			var offset = 3;

			var date = (to_date - new TimeSpan(offset * -1, 0, 0)).ToString();
			return date;
		}

		public IReadOnlyCollection<Email> ListEmailsFromDB(string statusName)
		{
			var emails = this.context.Emails
				.Include(p => p.Attachments)
				.Include(p => p.Status)
				.Include(p => p.User)
				.Include(p=>p.AppStatus)
				.Where(s => s.Status.StatusName.Replace(" ","").ToLower() == statusName.ToLower()).ToList();

			return emails;
		}

		public IReadOnlyCollection<Email> ListEmailsForManagers()
		{
			var emails = this.context.Emails
				.Include(p => p.Attachments)
				.Include(p => p.Status)
				.Include(p => p.User)
				.Include(p => p.AppStatus)
				.Where(s => s.Status.StatusName.Replace(" ", "").ToLower() == EmailStatusesEnum.InvalidApplication.ToString().ToLower() || s.Status.StatusName.Replace(" ", "").ToLower() == EmailStatusesEnum.NotReviewed.ToString().ToLower()).ToList();

			return emails;
		}

		public IReadOnlyCollection<Email> ListEmailsForOperator(string userId , string status)
		{
			var emails = this.context.Emails
				.Include(p => p.Attachments)
				.Include(p => p.Status)
				.Include(p => p.AppStatus)
				.Where(s => s.Status.StatusName == status && s.UserId == userId).ToList();

			return emails;
		}

		public string UserName(string userId)
		{
			var user = this.context.Users.FirstOrDefault(p => p.Id == userId);
			if (user == null)
			{
				return "Invalid user";
			}

			return user.UserName;
		}
	}
}
