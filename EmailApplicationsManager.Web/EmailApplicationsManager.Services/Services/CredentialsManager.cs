﻿using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Services.DTO;
using EmailApplicationsManager.Web.Data;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Services.Services
{
	public class CredentialsManager : ICredentialsManager
	{
		private readonly EmailAppContext context;

		public CredentialsManager(EmailAppContext context)
		{
			this.context = context;
		}

		public void RemoveTokens(GmailCredentials credentialsToRemove)
		{
			context.GmailCredentials.Remove(credentialsToRemove);
			context.SaveChanges();
		}

		public async Task SeedTokens (GmailCredentials credentials)
		{
			
			await context.GmailCredentials.AddAsync(credentials);
			await context.SaveChangesAsync();
		}

		public async Task<GmailCredentials> GetAsync()
		{
			var credentials = await context.GmailCredentials.FirstOrDefaultAsync();
			return credentials;
		}
	}
}
