﻿using EmailApplicationsManager.Repo.Contracts;
using EmailApplicationsManager.Services.Cypher;
using EmailApplicationsManager.Services.TestGmail;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Services.Services
{
	public class MailSyncer : IHostedService
    {
		private readonly ILogger logger;
        private readonly IServiceProvider serviceProvider;
        private Timer timer;

        public MailSyncer(ILogger<MailSyncer> logger, IServiceProvider serviceProvider)
        {
            this.logger = logger;
            this.serviceProvider = serviceProvider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            this.logger.LogInformation("Timed Background Service is starting.");
			
            this.timer = new Timer(GetNewEmailsFromGmail, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(30));

            return Task.CompletedTask;
        }

		private void GetNewEmailsFromGmail(object state)
		{
			using (var scope = this.serviceProvider.CreateScope())
			{
				var gmailUserDataService = scope.ServiceProvider.GetRequiredService<ICredentialsManager>();
				var gmailApiService = scope.ServiceProvider.GetRequiredService<IEmailServices>();
				var cipherService = scope.ServiceProvider.GetRequiredService<IStringCipher>();
				var repo = scope.ServiceProvider.GetRequiredService<ICredentialsRepo>();

				if (repo.AccessTokenExist().GetAwaiter().GetResult())
				{
					var userData = gmailUserDataService.GetAsync().GetAwaiter().GetResult();
					var userDataToRemove = userData;

					if ((userData.ExpiresOn - DateTime.Now).TotalMinutes < 5)
					{
						var newAccessDTO = gmailApiService.RenewAccessTokenAsync(userData.RefreshToken).GetAwaiter().GetResult();

						userData = gmailApiService.UpdateAccessToken(newAccessDTO).GetAwaiter().GetResult();
						gmailUserDataService.RemoveTokens(userDataToRemove);
						gmailUserDataService.SeedTokens(userData).GetAwaiter().GetResult();
					}

					gmailApiService.ListMessages(userData).GetAwaiter().GetResult();
				}

				//this.logger.LogInformation("Scoped service id: " + service.Id);
			}
		}

		public Task StopAsync(CancellationToken cancellationToken)
        {
            this.logger.LogInformation("Timed Background Service is stopping.");

            this.timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
    }
}
