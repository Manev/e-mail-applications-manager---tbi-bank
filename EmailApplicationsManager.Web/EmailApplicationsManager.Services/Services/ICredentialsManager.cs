﻿using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Services.DTO;
using EmailApplicationsManager.Web.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Services.Services
{
	public interface ICredentialsManager
	{
		void RemoveTokens(GmailCredentials credentialsToRemove);

		Task SeedTokens(GmailCredentials credentials);

		Task<GmailCredentials> GetAsync();
	}
}
