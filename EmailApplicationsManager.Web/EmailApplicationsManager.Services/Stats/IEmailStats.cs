﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmailApplicationsManager.Services.Stats
{
	public interface IEmailStats
	{
		int NotReviewedCount();


		int NewCount();


		int OpenCount();


		int ClosedCount();


		int ApprovedCount();


		int RejectedCount();
	}
}
