﻿using EmailApplicationsManager.Common.Extensions;
using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Web.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmailApplicationsManager.Services.Stats
{
	public class EmailStats : IEmailStats
	{
		private readonly EmailAppContext context;

		public EmailStats(EmailAppContext context)
		{
			this.context = context;
		}

		public int NotReviewedCount()
		{
			var count = context.Emails.Include(e => e.Status).Where(e => e.Status.StatusName.Replace(" ","").ToLower() == EmailStatusesEnum.NotReviewed.ToString().ToLower()).ToList().Count;
			return count;
		}

		public int NewCount()
		{
			var count = context.Emails.Include(e => e.Status).Where(e => e.Status.StatusName == EmailStatusesEnum.New.ToString()).ToList().Count;
			return count;
		}

		public int OpenCount()
		{
			var count = context.Emails.Include(e => e.Status).Where(e => e.Status.StatusName == EmailStatusesEnum.Open.ToString()).ToList().Count;
			return count;
		}

		public int ClosedCount()
		{
			var count = context.Emails.Include(e => e.Status).Where(e => e.Status.StatusName == EmailStatusesEnum.Closed.ToString()).ToList().Count;
			return count;
		}

		public int ApprovedCount()
		{
			var count = context.Emails.Include(e => e.AppStatus).Where(e => e.AppStatus.AppStatusName == ApplicationStatuseEnum.Approved.ToString()).ToList().Count;
			return count;
		}

		public int RejectedCount()
		{
			var count = context.Emails.Include(e => e.AppStatus).Where(e => e.AppStatus.AppStatusName == ApplicationStatuseEnum.Rejected.ToString()).ToList().Count;
			return count;
		}
	}
}
