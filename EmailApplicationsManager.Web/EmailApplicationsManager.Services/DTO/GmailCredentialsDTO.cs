﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmailApplicationsManager.Services.DTO
{
	public class GmailCredentialsDTO
	{

		[JsonProperty ("refresh_token")]
		public string RefreshToken { get; set; }

		[JsonProperty("access_token")]
		public string AccessToken { get; set; }

		[JsonProperty("expires_in")]
		public int ExpiresInSeconds { get; set; }
	}
}
