﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Services.Cypher
{
	public interface IStringCipher
	{
		 string Encrypt(string plainText);

		string Decrypt(string plainText);
	}
}
