﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EmailApplicationsManager.Web.Models;
using EmailApplicationsManager.Services.TestGmail;
using Google.Apis.Gmail.v1;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using EmailApplicationsManager.Services.DTO;
using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Services.Services;
using EmailApplicationsManager.Repo.Contracts;
using EmailApplicationsManager.Repo;
using EmailApplicationsManager.Services.Stats;

namespace EmailApplicationsManager.Web.Controllers
{
	public class HomeController : Controller
	{
		private readonly IEmailServices Services;
		private readonly ICredentialsManager CredentialServices;
		private readonly ICredentialsRepo repo;
		private readonly IEmailStats emailsCounter;

		public HomeController(IEmailServices services, ICredentialsManager credentialServices, ICredentialsRepo repo, IEmailStats emailsCounter)
		{
			this.Services = services;
			this.CredentialServices = credentialServices;
			this.repo = repo;
			this.emailsCounter = emailsCounter;
		}

		public IActionResult GoogleLogin(string code)
		{
			var sb = new StringBuilder()
				.Append("https://accounts.google.com/o/oauth2/v2/auth?")
				.Append("scope=https://www.googleapis.com/auth/gmail.readonly")
				.Append("&access_type=offline")
				.Append("&include_granted_scopes=true")
				.Append("&response_type=code")
				//.Append("&redirect_uri=http://localhost:5432/google-callback")
				.Append("&redirect_uri=http://emailappmanager2.azurewebsites.net/google-callback")
				.Append("&client_id=708903751266-ir6gpg7ck4d1okr8mimfqmqtj0qqeqjg.apps.googleusercontent.com");

			return Redirect(sb.ToString());
		}

		[Route("google-callback")]
		public async Task<IActionResult> GoogleCallback(string code)
		{
			var client = new HttpClient();
			var content = new FormUrlEncodedContent(new[]
			{
				new KeyValuePair<string,string>("code",code),
				new KeyValuePair<string,string>("client_id","708903751266-ir6gpg7ck4d1okr8mimfqmqtj0qqeqjg.apps.googleusercontent.com"),
				new KeyValuePair<string,string>("client_secret","g8XjzM9InQV4siZ66xTtPJDZ"),
				new KeyValuePair<string,string>("redirect_uri","http://emailappmanager2.azurewebsites.net/google-callback"),
			//	new KeyValuePair<string,string>("redirect_uri","http://localhost:5432/google-callback"),
				new KeyValuePair<string,string>("grant_type","authorization_code"),
			});

			var res = await client.PostAsync("https://oauth2.googleapis.com/token", content);

			if (!res.IsSuccessStatusCode)
			{
				return Json(await res.Content.ReadAsStringAsync());
			}
			else
			{
				var userDataDTO = JsonConvert.DeserializeObject<GmailCredentialsDTO>(await res.Content.ReadAsStringAsync());
				var gmailCredentials = new GmailCredentials
				{
					AccessToken = userDataDTO.AccessToken,
					RefreshToken = userDataDTO.RefreshToken,
					ExpiresOn = DateTime.Now.AddSeconds(userDataDTO.ExpiresInSeconds)
				};
				await CredentialServices.SeedTokens(gmailCredentials);
				//return Json(await TryReadGmail(client, userData));
				return RedirectToAction("Login", "Account");
			}

		}

		public async Task<bool> TryReadGmail(HttpClient client, GmailCredentialsDTO userData)
		{
			client.DefaultRequestHeaders.Add("Authorization", "Bearer " + userData.AccessToken);

			var res = await client.GetAsync("https://www.googleapis.com/gmail/v1/users/me/messages");

			var content = await res.Content.ReadAsStringAsync();

			return res.IsSuccessStatusCode;
		}

		public IActionResult Index()
		{
			if (!repo.AccessTokenExist().GetAwaiter().GetResult())
			{
				return RedirectToAction("GoogleLogin");
			}

			ViewBag.emailsCounter = emailsCounter;

			return View();
		}

		public IActionResult About()
		{
			ViewData["Message"] = "Your application description page.";

			//var messages = Services.ListMessages(GmailServices, "me");

			return View();
		}

		public IActionResult Contact()
		{
			ViewData["Message"] = "Your contact page.";

			return View();
		}

		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}

		public IActionResult ListEmails()
		{
			return View();
		}
	}
}
