﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmailApplicationsManager.Services.AuditLogger;
using Microsoft.AspNetCore.Mvc;

namespace EmailApplicationsManager.Web.Controllers
{
    public class LogController : Controller
    {
		private readonly IAuditLogger logger;

		public LogController(IAuditLogger logger)
		{
			this.logger = logger;
		}

        public IActionResult Index()
        {
			ViewBag.logger = logger;

            return View("Index");
        }
    }
}