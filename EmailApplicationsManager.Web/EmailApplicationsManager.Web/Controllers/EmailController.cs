﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Repo;
using EmailApplicationsManager.Repo.Contracts;
using EmailApplicationsManager.Services.AuditLogger;
using EmailApplicationsManager.Services.Cypher;
using EmailApplicationsManager.Services.TestGmail;
using EmailApplicationsManager.Services.Update;
using EmailApplicationsManager.Web.Mappers;
using EmailApplicationsManager.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EmailApplicationsManager.Web.Controllers
{
    public class EmailController : Controller
    {

		private readonly IEmailServices mailServices;
		private readonly IViewModelMapper<IReadOnlyCollection<Email>, HomeViewModel> mailMapper;
		private readonly IViewModelMapper<Email, EmailViewModel> mapper;
		private readonly IStringCipher cipher;
		private readonly IUpdateServices update;
		private readonly IApplicationStatusesRepo repo;
		private readonly IUserRepo userRepo;
		private readonly IAuditLogger logger;

		public EmailController(IEmailServices mailServices, IViewModelMapper<IReadOnlyCollection<Email>,HomeViewModel> mailMapper, IStringCipher cipher, IViewModelMapper<Email, EmailViewModel> mapper,IUpdateServices update, IApplicationStatusesRepo repo, IUserRepo userRepo, IAuditLogger logger)
		{
			this.mailServices = mailServices;
			this.mailMapper = mailMapper;
			this.cipher = cipher;
			this.mapper = mapper;
			this.update = update;
			this.repo = repo;
			this.userRepo = userRepo;
			this.logger = logger;
		}

		[HttpGet]
		public IActionResult FindNewEmails()
		{
			try
			{ 

				var foundEmails = this.mailServices.ListEmailsFromDB(EmailStatusesEnum.New.ToString());
				foreach (var email in foundEmails)
				{
					email.Sender = cipher.Decrypt(email.Sender);
				}
				HomeViewModel vm = this.mailMapper.MapFrom(foundEmails);

				return View("View", vm);
			}
			catch (Exception)
			{
				throw;
			}
		}

		[HttpGet]
		public IActionResult FindOpenEmails()
		{
			try
			{
				if (User.IsInRole("Operator"))
				{
					string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
					var foundEmails = this.mailServices.ListEmailsForOperator(userId, EmailStatusesEnum.Open.ToString());

					foreach (var email in foundEmails)
					{
						email.Sender = cipher.Decrypt(email.Sender);
					}
					HomeViewModel vm = this.mailMapper.MapFrom(foundEmails);

					foreach (var mail in vm.Emails)
					{
						mail.UserName = userRepo.GetUserName(mail.UserName);
					}

					return View("View", vm);
				}
				else
				{
					var foundEmails = this.mailServices.ListEmailsFromDB(EmailStatusesEnum.Open.ToString());
					foreach (var email in foundEmails)
					{
						email.Sender = cipher.Decrypt(email.Sender);
					}
					HomeViewModel vm = this.mailMapper.MapFrom(foundEmails);

					foreach (var mail in vm.Emails)
					{
						mail.UserName = userRepo.GetUserName(mail.UserName);
					}

					return View("View", vm);
				}
			}
			catch (Exception)
			{
				throw;
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult OpenToClose(EmailViewModel model)
		{
			var mail = this.mailServices.GetEmail(model.Id);

			var oldStatus = mail.Status.StatusName;

			if (model.Status == null)
			{
				update.ChangeStatus(model.Id, EmailStatusesEnum.Closed.ToString(), model.Body);
			}
			else
			{
				update.ChangeStatus(model.Id, model.Status, model.Body);
			}

			string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

			update.ApplicationClosing(model.Id, model.AppStatus, userId);
			
			logger.Log(userId, model.Id, $"Application {model.AppStatus}", oldStatus, model.Status);

			return RedirectToAction("FindClosedEmails", "Email");
		}


		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult NewToOpen(EmailViewModel model)
		{
			var mail = this.mailServices.GetEmail(model.Id);

			var oldStatus = mail.Status.StatusName;

			string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

			if (model.Status == EmailStatusesEnum.Open.ToString())
			{
				update.ChangeStatus(model.Id, model.Status, model.Body);

				update.AddCustomerInformation(model.Id, model.EGN, model.PhoneNumber, userId);

				logger.Log(userId, model.Id, "Status change", oldStatus, model.Status);

				return RedirectToAction("FindOpenEmails", "Email");
			}

			update.ChangeStatus(model.Id, model.Status, model.Body);

			logger.Log(userId, model.Id, "Status change", oldStatus, model.Status);

			return RedirectToAction("FindNotReviewedEmails", "Email");
		}

		[HttpGet]
		public IActionResult FindClosedEmails()
		{
			try
			{
				if (User.IsInRole("Operator"))
				{
					string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
					var foundEmails = this.mailServices.ListEmailsForOperator(userId, EmailStatusesEnum.Closed.ToString());

					foreach (var email in foundEmails)
					{
						email.Sender = cipher.Decrypt(email.Sender);
					}
					HomeViewModel vm = this.mailMapper.MapFrom(foundEmails);
					foreach (var mail in vm.Emails)
					{
						mail.UserName = userRepo.GetUserName(userId);
					}

					return View("View", vm);
				}
				else
				{
					var foundEmails = this.mailServices.ListEmailsFromDB(EmailStatusesEnum.Closed.ToString());
					foreach (var email in foundEmails)
					{
						email.Sender = cipher.Decrypt(email.Sender);
					}
					HomeViewModel vm = this.mailMapper.MapFrom(foundEmails);

					foreach (var mail in vm.Emails)
					{
						mail.UserName = userRepo.GetUserName(mail.UserName);
					}

					return View("View", vm);
				}
			}
			catch (Exception)
			{
				throw;
			}
		}

		[HttpGet]
		public IActionResult FindNotReviewedEmails()
		{
			try
			{
				if (User.IsInRole("Operator"))
				{
					var foundEmails = this.mailServices.ListEmailsFromDB(EmailStatusesEnum.NotReviewed.ToString());
					foreach (var email in foundEmails)
					{
						if (email.UserId == null)
						{
							email.UserId = mailServices.UserName(email.UserId);
						}
						email.Sender = cipher.Decrypt(email.Sender);
					}
					HomeViewModel vm = this.mailMapper.MapFrom(foundEmails);

					return View("View", vm);
				}
				else
				{
					var foundEmails = this.mailServices.ListEmailsForManagers();
					foreach (var email in foundEmails)
					{
						if (email.UserId == null)
						{
							email.UserId = mailServices.UserName(email.UserId);
						}
						email.Sender = cipher.Decrypt(email.Sender);
					}
					HomeViewModel vm = this.mailMapper.MapFrom(foundEmails);

					return View("View", vm);
				}

			}
			catch (Exception)
			{
				throw;
			}
		}

		[HttpGet]
		public IActionResult Details(int id)
		{
			var mail = this.mailServices.GetEmail(id);
			
			mail.Body = update.GetBody(mail.GmailId).GetAwaiter().GetResult();
			mail.Sender = cipher.Decrypt(mail.Sender);

			if (mail == null)
				return NotFound();

			IEnumerable<SelectListItem> StatusNotReviewed = new List<SelectListItem>()
			{
				new SelectListItem() { Text = "New", Value = "New"},
				new SelectListItem() { Text = "Invalid Application", Value = "Invalid Application"}
			};

			IEnumerable<SelectListItem> ManagerStatusNew = new List<SelectListItem>()
			{
				new SelectListItem() { Text = "Open", Value = "Open"},
				new SelectListItem() { Text = "Not Reviewed", Value = "Not Reviewed"}
			};

			IEnumerable<SelectListItem> OperatorStatusNew = new List<SelectListItem>()
			{
				new SelectListItem() { Text = "Open", Value = "Open"}
			};

			IEnumerable<SelectListItem> ManagerStatusOpen = new List<SelectListItem>()
			{
				new SelectListItem() { Text = "Closed", Value = "Closed"},
				new SelectListItem() { Text = "New", Value = "New"}
			};

			IEnumerable<SelectListItem> ManagerStatusClosed = new List<SelectListItem>()
			{
				new SelectListItem() { Text = "New", Value = "New"}
			};

			IEnumerable<SelectListItem> Statuses = repo.GetAppStatuses();

			ViewBag.ManagerStatusClosed = ManagerStatusClosed;
			ViewBag.Statuses = Statuses;
			ViewBag.ManagerStatusOpen = ManagerStatusOpen;
			ViewBag.ManagerStatusNew = ManagerStatusNew;
			ViewBag.OperatorStatusNew = OperatorStatusNew;
			ViewBag.StatusNotReviewed = StatusNotReviewed;
			return View(this.mapper.MapFrom(mail));
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Details(EmailViewModel model)
		{
			string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

			var mail = this.mailServices.GetEmail(model.Id);

			var oldStatus = mail.Status.StatusName;

			if (model.Status == null)
			{
				update.ChangeStatus(model.Id, EmailStatusesEnum.NotReviewed.ToString().ToLower(), model.Body);

				logger.Log(userId, model.Id, "Status change", oldStatus, model.Status);

				return RedirectToAction("FindNotReviewedEmails", "Email");

			}

			update.ChangeStatus(model.Id, model.Status, model.Body);

			logger.Log(userId, model.Id, "Status change", oldStatus, model.Status);

			if (model.Status.Replace(" ", "").ToLower() == EmailStatusesEnum.InvalidApplication.ToString().ToLower())
			{
				return RedirectToAction("FindNotReviewedEmails", "Email");
			}

				return RedirectToAction("FindNewEmails", "Email");
		}
	}
	}