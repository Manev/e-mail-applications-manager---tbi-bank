﻿using EmailApplicationsManager.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Web.Models
{
	public class EmailViewModel
	{
		public int Id { get; set; }

		public string Sender { get; set; }

		public DateTime Date { get; set; }

		public string Subject { get; set; }

		public string Body { get; set; }

		public string Status { get; set; }

		public string AppStatus { get; set; }

		[Required]
		[MinLength(10, ErrorMessage = "This field must be at least 10 characters long")]
		[MaxLength(10, ErrorMessage = "This field must be at most 10 characters long")]
		[RegularExpression("^[0-9]*$", ErrorMessage = "EGN must be numeric")]
		public string EGN { get; set; }

		[Required]
		[MinLength(10, ErrorMessage = "This field must be at least 10 characters long")]
		[MaxLength(10, ErrorMessage = "This field must be at most 10 characters long")]
		[RegularExpression("^[0-9]*$", ErrorMessage = "EGN must be numeric")]
		public string PhoneNumber { get; set; }

		public DateTime SinceCurrentStatus { get; set; }

		public int AttachmentCount { get; set; }

		public ICollection<Attachment> Attachments { get; set; }

		public string UserName { get; set; }
	}
}
