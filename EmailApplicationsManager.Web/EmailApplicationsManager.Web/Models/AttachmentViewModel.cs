﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Web.Models
{
	public class AttachmentViewModel
	{
		public string Id { get; set; }

		public int EmailId {get; set;}

		public string FileName { get; set; }

		public double? Size { get; set; }
	}
}
