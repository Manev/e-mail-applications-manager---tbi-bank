﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Web.Models
{
	public class HomeViewModel
	{
		public IReadOnlyList<EmailViewModel> Emails { get; set; }

		public IReadOnlyList<AttachmentViewModel> Attachment { get; set; }
	}
}
