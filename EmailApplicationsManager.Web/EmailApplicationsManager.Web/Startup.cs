﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using EmailApplicationsManager.Web.Data;
using EmailApplicationsManager.Web.Services;
using EmailApplicationsManager.Data.Models;
using Google.Apis.Gmail.v1;
using Google.Apis.Services;
using Google.Apis.Auth.OAuth2;
using System.IO;
using System.Threading;
using Google.Apis.Util.Store;
using EmailApplicationsManager.Services.TestGmail;
using EmailApplicationsManager.Repo.Contracts;
using EmailApplicationsManager.Repo;
using EmailApplicationsManager.Data.Abstract;
using Microsoft.AspNetCore.Hosting.Server;
using EmailApplicationsManager.Services.Services;
using EmailApplicationsManager.Services.Cypher;
using EmailApplicationsManager.Web.Mappers;
using Microsoft.AspNetCore.Http;
using EmailApplicationsManager.Services.Update;
using EmailApplicationsManager.Services.AuditLogger;
using EmailApplicationsManager.Services.Stats;

namespace EmailApplicationsManager.Web
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<EmailAppContext>(options =>
				options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));

			services.AddIdentity<User, IdentityRole>()
				.AddEntityFrameworkStores<EmailAppContext>()
				.AddDefaultTokenProviders();

			services.AddTransient<IEmailServices, EmailServices>();
			services.AddTransient<IRolesRepo, RolesRepo>();
			services.AddTransient<IUserRepo, UserRepo>();
			services.AddTransient<IUpdateServices, UpdateServices>();
			services.AddTransient<IEmailAppContext, EmailAppContext>();
			services.AddTransient<ICredentialsManager, CredentialsManager>();
			services.AddTransient<ICredentialsRepo, CredentialsRepo>();
			services.AddTransient<IStringCipher, StringCipher>();
			services.AddTransient<IAuditLogger, AuditLogger>();
			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
			services.AddTransient<IApplicationStatusesRepo, ApplicationStatusesRepo>();
			services.AddTransient<IEmailStats, EmailStats>();
			services.AddHostedService<MailSyncer>();

			services.AddCustomMappers();
			services.AddMvc().SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_2_2);

		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
			}

			app.UseStaticFiles();

			app.UseAuthentication();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
				name: "default",
				template: "{controller=Home}/{action=Index}/{id?}");
			});
		}
	}
}
