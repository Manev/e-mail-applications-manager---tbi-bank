﻿using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Web.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Web.Mappers
{
	public static class MapperRegistration
	{
		public static IServiceCollection AddCustomMappers(this IServiceCollection services)
		{
			services.AddSingleton<IViewModelMapper<Email, EmailViewModel>, EmailViewModelMapper>();
			services.AddSingleton<IViewModelMapper<Attachment, AttachmentViewModel>, AttachmentViewModelMapper>();
			services.AddSingleton<IViewModelMapper<IReadOnlyCollection<Email>, HomeViewModel>, HomeViewModelMapper>();


			return services;
		}
	}
}
