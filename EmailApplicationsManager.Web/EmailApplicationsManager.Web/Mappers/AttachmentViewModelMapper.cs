﻿using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Web.Mappers
{
	public class AttachmentViewModelMapper : IViewModelMapper<Attachment, AttachmentViewModel>
	{
		public AttachmentViewModel MapFrom(Attachment entity)
		=> new AttachmentViewModel
		{
			Id = entity.Id.ToString(),
			EmailId = entity.Email.Id,
			FileName = entity.FileName,
			Size = entity.SizeInMb
		};
	}
}
