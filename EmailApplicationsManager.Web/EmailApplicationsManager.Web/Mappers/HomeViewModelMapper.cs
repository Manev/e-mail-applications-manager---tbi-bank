﻿using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Web.Mappers
{
	public class HomeViewModelMapper : IViewModelMapper<IReadOnlyCollection<Email>, HomeViewModel>
	{
		private readonly IViewModelMapper<Email, EmailViewModel> mailMapper;
		private readonly IViewModelMapper<Attachment, AttachmentViewModel> attachmentMapper;

		public HomeViewModelMapper(
			IViewModelMapper<Email, EmailViewModel> mailMapper,
			IViewModelMapper<Attachment, AttachmentViewModel> attachmentMapper)
		{
			this.mailMapper = mailMapper ?? throw new ArgumentNullException(nameof(mailMapper));
			this.attachmentMapper = attachmentMapper ?? throw new ArgumentNullException(nameof(attachmentMapper));
		}

		public HomeViewModel MapFrom(IReadOnlyCollection<Email> entity)
			 => new HomeViewModel
			 {
				 //Emails = mailMapper.MapFrom(entity),

				 //Attachment = entity.SelectMany(p=> p.Attachments ,this.attachmentMapper.MapFrom).ToList(),

				 Attachment = entity.SelectMany(t => t.Attachments,
				 						(_, p) => this.attachmentMapper.MapFrom(p))
				 			 .ToList(),
				 Emails = entity.Select(this.mailMapper.MapFrom).ToList()

			 };
	}
}
