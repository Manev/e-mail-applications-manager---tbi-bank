﻿using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailApplicationsManager.Web.Mappers
{
	public class EmailViewModelMapper : IViewModelMapper<Email, EmailViewModel>
	{
			public EmailViewModel MapFrom(Email entity)
		=> new EmailViewModel
		{
			Id = entity.Id,
			Sender = entity.Sender,
			Date = entity.DateReceived,
			Subject = entity.Subject,
			Body = entity.Body,
			Status = entity.Status.StatusName,
			SinceCurrentStatus = entity.SetInCurrentStatusOn,
			AttachmentCount =  entity.Attachments.Count,
			Attachments = entity.Attachments,
			EGN = entity.EGN,
			PhoneNumber = entity.PhoneNumber,
			UserName = entity.UserId,
			AppStatus = entity.AppStatus.AppStatusName
		};
	}
}
