﻿using System.IO;
using System.Linq;
using System.Reflection;
using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Web.Data;
using log4net;
using log4net.Config;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Quartz;

namespace EmailApplicationsManager.Web
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
			XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

			//ILog log = log4net.LogManager.GetLogger(typeof(Program));

			//log.Info("logging");

			var host = BuildWebHost(args);

			SeedDatabase(host);
			
			host.Run();
		}

		private static void SeedDatabase(IWebHost host)
		{
			using (var scope = host.Services.CreateScope())
			{
				var dbContext = scope.ServiceProvider.GetRequiredService<EmailAppContext>();

				if (dbContext.Roles.Any(r => r.Name == "Admin") && dbContext.Roles.Any(r => r.Name == "Operator") && dbContext.Roles.Any(r => r.Name == "Manager"))
				{
					return;
				}

				var userManager = scope.ServiceProvider.GetRequiredService<UserManager<User>>();
				var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

				roleManager.CreateAsync(new IdentityRole { Name = "Admin" }).Wait();
				roleManager.CreateAsync(new IdentityRole { Name = "Operator" }).Wait();
				roleManager.CreateAsync(new IdentityRole { Name = "Manager" }).Wait();

				var adminUser = new User { UserName = "admin@admin.admin", Email = "admin@admin.admin" };
				userManager.CreateAsync(adminUser, "Admin123@").Wait();

				userManager.AddToRoleAsync(adminUser, "Admin").Wait();
			}
		}
		
		public static IWebHost BuildWebHost(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.UseIISIntegration()
				.UseStartup<Startup>()
				.Build();
	}
}
