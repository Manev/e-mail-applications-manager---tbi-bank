﻿using EmailApplicationsManager.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmailApplicationsManager.Data.Abstract
{
	public interface IEmailAppContext
	{
		DbSet<Email> Emails { get; set; }

		DbSet<EmailStatus> EmailStatuses { get; set; }
	}
}
