﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using EmailApplicationsManager.Data.Models;
using EmailApplicationsManager.Data.Abstract;
using Newtonsoft.Json;
using System.Reflection;
using System.IO;
using EmailApplicationsManager.Common.Extensions;
using System.ComponentModel.DataAnnotations;

namespace EmailApplicationsManager.Web.Data
{
	public class EmailAppContext : IdentityDbContext<User>, IEmailAppContext
	{
		public EmailAppContext(DbContextOptions<EmailAppContext> options)
			: base(options)
		{
		}

		public DbSet<Email> Emails { get; set; }
		public DbSet<EmailStatus> EmailStatuses { get; set; }
		public DbSet<ApplicationStatus> ApplicationStatuses { get; set; }
		public DbSet<Attachment> Attachments { get; set; }
		public DbSet<GmailCredentials> GmailCredentials { get; set; }
		public DbSet<Log> Logs { get; set; }

		private void LoadStatusesInDB(ModelBuilder modelbuilder)
		{
			List<EmailStatus> statuses = new List<EmailStatus>();
			foreach (EmailStatusesEnum item in Enum.GetValues(typeof(EmailStatusesEnum)))
			{
				string name = string.Empty;

				var displayAttribute = item.GetAttribute<DisplayAttribute>();
				if (displayAttribute != null)
				{
					name = displayAttribute.Name;
				}
				else
				{
					name = item.ToString();
				}

				statuses.Add(new EmailStatus()
				{
					Id = (int)item,
					StatusName = name
				});
			}

			List<ApplicationStatus> appStatus = new List<ApplicationStatus>();
			foreach (ApplicationStatuseEnum status in Enum.GetValues(typeof(ApplicationStatuseEnum)))
			{
				string name = string.Empty;

				var displayAttribute = status.GetAttribute<DisplayAttribute>();
				if (displayAttribute != null)
				{
					name = displayAttribute.Name;
				}
				else
				{
					name = status.ToString();
				}

				appStatus.Add(new ApplicationStatus()
				{
					Id = (int)status,
					AppStatusName = name
				});
			}

			modelbuilder.Entity<EmailStatus>().HasData(statuses);
			modelbuilder.Entity<ApplicationStatus>().HasData(appStatus);
		}
		
		protected override void OnModelCreating(ModelBuilder builder)
		{
			LoadStatusesInDB(builder);

			base.OnModelCreating(builder);
		}
	}
}
