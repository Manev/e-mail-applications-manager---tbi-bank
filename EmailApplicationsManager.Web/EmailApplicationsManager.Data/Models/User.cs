﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace EmailApplicationsManager.Data.Models
{
	public class User : IdentityUser
	{
		public string FirstName { get; set; }

		public string LastName { get; set; }

		[DefaultValue("false")]
		public bool HasLogged {get; set;}
	}
}
