﻿using EmailApplicationsManager.Data.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmailApplicationsManager.Data.Models
{
	public class GmailCredentials : DomainClassAbstract
	{
		public string RefreshToken { get; set; }

		public string AccessToken { get; set; }

		public DateTime ExpiresOn { get; set; }
	}
}
