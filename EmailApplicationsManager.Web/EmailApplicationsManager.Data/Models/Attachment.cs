﻿using EmailApplicationsManager.Data.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmailApplicationsManager.Data.Models
{
	public class Attachment : DomainClassAbstract
	{
		public string GmailId { get; set; }

		public int EmailId { get; set; }

		public Email Email { get; set; }

		public string FileName { get; set; }

		public double? SizeInMb { get; set; }
	}
}
