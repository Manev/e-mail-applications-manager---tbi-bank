﻿using EmailApplicationsManager.Data.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EmailApplicationsManager.Data.Models
{
	public class Email : DomainClassAbstract
	{
		public string GmailId { get; set; }

		public int StatusId { get; set; }

		public EmailStatus Status { get; set; }

		public int? AppStatusId { get; set; }

		public ApplicationStatus AppStatus { get; set; }

		public string Sender { get; set; }

		public DateTime DateReceived { get; set; }

		public string Subject { get; set; }

		public string Body { get; set; }

		public int AttachmentsCount { get; set; }

		public int AttachmentsSize { get; set; }

		public ICollection<Attachment> Attachments { get; set; }

		public DateTime? InitialRegistrationInSystemOn { get; set; }

		public DateTime SetInCurrentStatusOn { get; set; }

		public DateTime? SetInTerminalState { get; set; }

		public string EGN { get; set; }

		public string PhoneNumber { get; set; }

		public User User { get; set; }

		public string UserId { get; set; }
	}
}
