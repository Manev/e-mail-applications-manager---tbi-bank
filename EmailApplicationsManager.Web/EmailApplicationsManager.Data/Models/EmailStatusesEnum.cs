﻿using System.ComponentModel.DataAnnotations;

namespace EmailApplicationsManager.Data.Models
{
	public enum EmailStatusesEnum
	{
		[Display(Name ="Not Reviewed")]
		NotReviewed = 1,

		[Display(Name = "Invalid Application")]
		InvalidApplication = 2,

		New = 3,
		Open = 4,
		Closed = 5
	}
}
