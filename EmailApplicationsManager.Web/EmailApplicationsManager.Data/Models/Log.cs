﻿using EmailApplicationsManager.Data.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmailApplicationsManager.Data.Models
{
	public class Log : DomainClassAbstract
	{
		public DateTime Date { get; set; }

		public string Info { get; set; }

		public User User { get; set; }

		public string UserId { get; set; }

		public Email Email { get; set; }

		public int EmailId { get; set; }

		public string LastStatus { get; set; }

		public string NewStatus { get; set; }
	}
}
