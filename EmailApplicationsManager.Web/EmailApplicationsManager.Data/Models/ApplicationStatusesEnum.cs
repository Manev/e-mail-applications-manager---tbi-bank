﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EmailApplicationsManager.Data.Models
{
	public enum ApplicationStatuseEnum
	{
		[Display(Name = "Approved")]
		Approved = 1,

		[Display(Name = "Rejected")]
		Rejected = 2,

		[Display(Name = "Not Reviewed")]
		NotReviewed = 3,
	}
}
