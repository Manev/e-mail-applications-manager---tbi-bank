﻿using EmailApplicationsManager.Data.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmailApplicationsManager.Data.Models
{
	public class EmailStatus : DomainClassAbstract
	{
		public string StatusName { get; set; }

		public IReadOnlyCollection<Email> Emails { get; set; }
	}
}
