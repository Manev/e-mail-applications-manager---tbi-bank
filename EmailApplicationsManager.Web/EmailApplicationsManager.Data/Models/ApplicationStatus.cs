﻿using EmailApplicationsManager.Data.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmailApplicationsManager.Data.Models
{
	public class ApplicationStatus : DomainClassAbstract
	{
		public string AppStatusName { get; set; }

		public IReadOnlyCollection<Email> Emails { get; set; }
	}
}
