﻿using System;
using System.Linq;
using System.Reflection;

namespace EmailApplicationsManager.Common.Extensions
{
	public static class EnumExtensions
	{ 
		public static TAttribute GetAttribute<TAttribute>(this Enum enumValue)
				where TAttribute : Attribute
		{
			var member = enumValue.GetType()
							.GetMember(enumValue.ToString())
							.FirstOrDefault();
			if (member != null)
			{
				return member.GetCustomAttribute<TAttribute>();
			}

			return null;
		}
	}
}
